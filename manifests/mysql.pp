class mymodule::mysql
{
  # MYSQL
  class { '::mysql::server':
    root_password    => 'vagrantpass',
  }

  mysql::db { 'mympwar':
    user     => 'mympwar_db',
    password => 'mympwar_db_password',
  }

  mysql::db { 'mympwar_test':
    user     => 'mympwar_test_db',
    password => 'mympwar_test_db_password',
  }
}