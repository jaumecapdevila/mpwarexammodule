class mymodule::createfiles
{
  file { '/var/www/myproject/index.php':
    ensure  => 'present',
    source  => "puppet:///modules/mymodule/index.php",
    mode    => '0644',
  }

  file { '/var/www/project1/index.php':
    ensure  => 'present',
    source => "puppet:///modules/mymodule/info.php",
    mode    => '0644',
  }
}