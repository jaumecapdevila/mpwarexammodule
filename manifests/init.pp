class mymodule
{
  class {'::mymodule::packages': }
  class {'::mymodule::apache': }
  class {'::mymodule::mysql': }
  class {'::mymodule::php':
      require => Class['::mymodule::apache']
  }
  # Create files
  include mymodule::createfiles


}