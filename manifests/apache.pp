class mymodule::apache
{
  # Edit local /etc/hosts files to resolve some hostnames used on your application.
  host { 'localhost':
    ensure       => 'present',
    target       => '/etc/hosts',
    ip           => '127.0.0.1',
    host_aliases => [
      'mysql1',
      'memcached1'
    ]
  }

  class{ 'apache': }

  apache::vhost { 'myMpwar.prod':
    port    => '80',
    docroot       => '/var/www/myproject',
  }

  apache::vhost { 'myMpwar.dev':
    port    => '80',
    docroot       => '/var/www/myproject',
  }

  include apache::mod::php
}